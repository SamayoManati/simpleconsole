package com.darksidegames.simpleConsole;

import java.awt.KeyboardFocusManager;
import java.awt.KeyEventDispatcher;
import java.awt.event.KeyEvent;

public class Keyboard
{
	private static volatile boolean leftPressed = false;

	public static boolean isPressed(int key)
	{
		synchronized (Keyboard.class) {
			if (key == KeyEvent.VK_LEFT) return leftPressed;
			return false;
		}
	}

	public static void initKeyboard()
	{
		KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(new KeyEventDispatcher() {
			@Override
			public boolean dispatchKeyEvent(KeyEvent key)
			{
				synchronized (Keyboard.class) {
					switch (key.getID()) {
						case KeyEvent.KEY_PRESSED:
						if (key.getKeyCode() == KeyEvent.VK_LEFT) leftPressed = true;
						break;

						case KeyEvent.KEY_RELEASED:
						if (key.getKeyCode() == KeyEvent.VK_LEFT) leftPressed = false;
						break;
					}
					return false;
				}
			}
		});
	}
}