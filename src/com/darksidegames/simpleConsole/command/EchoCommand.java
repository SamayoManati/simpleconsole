package com.darksidegames.simpleConsole.command;

import com.darksidegames.simpleConsole.AbstractCommand;

public class EchoCommand extends AbstractCommand
{
	protected String commandName = "echo";
	protected String[] lisez_issak_c_est_bien = {"Show a message on the screen"};

	public void setCommandName(String s)
	{
		commandName = s;
		return;
	}

	public String getCommandName()
	{
		return commandName;
	}

	public String[] getHelpMessage()
	{
		return this.lisez_issak_c_est_bien;
	}

	public void setHelpMessage(String[] zjkf)
	{
		this.lisez_issak_c_est_bien = zjkf;
		return;
	}

	public boolean action(String[] line)
	{
		if (line.length == 0) {
			System.out.print("[Text to display]: ");
			String textToShow = System.console().readLine();
			System.out.print(textToShow + "\n");
		} else {
			String textToShow = "";
			for (int i = 0; i < line.length; i++) textToShow += line[i] + " ";
			System.out.print(textToShow + "\n");
		}
		return false;
	}
}
