package com.darksidegames.simpleConsole.command;

import com.darksidegames.simpleConsole.AbstractCommand;

public class ExitCommand extends AbstractCommand
{
	protected String commandName = "exit";
	protected String[] helpMessage = {"Exit the console"};

	public boolean action(String[] args)
	{
		System.out.print("Goodbye...\n");
		return true;
	}

	public String getCommandName()
	{
		return this.commandName;
	}

	public void setCommandName(String name)
	{
		this.commandName = name;
		return;
	}

	public String[] getHelpMessage()
	{
		return this.helpMessage;
	}

	public void setHelpMessage(String[] helpMessage)
	{
		this.helpMessage = helpMessage;
		return;
	}
}