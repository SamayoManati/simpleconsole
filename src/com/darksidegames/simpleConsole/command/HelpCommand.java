package com.darksidegames.simpleConsole.command;

import com.darksidegames.simpleConsole.AbstractCommand;
import com.darksidegames.simpleConsole.SimpleConsole;
import java.util.ArrayList;

public class HelpCommand extends AbstractCommand
{
	protected String commandName = "help";
	protected String[] helpMessage = {
		"Display an help message",
		"",
		"Arguments :",
		"none"
	};

	public boolean action(String[] args)
	{
		ArrayList<AbstractCommand> cmds = SimpleConsole.getRegisteredCommands();
		for (int i = 0; i < cmds.size(); i++) {
			String name = cmds.get(i).getCommandName();
			String[] help = cmds.get(i).getHelpMessage();
			String line = "  " + name;
			int baseSpacesCount = 10;
			int currentSpacesCount = Math.abs(baseSpacesCount - name.length());
			for (int j = 0; j < currentSpacesCount; j++) line += " ";
			line += help[0];
			line += "\n";
			if (help.length > 1) {
				for (int j = 1; j < help.length; j++) {
					int innerSpacesCount = baseSpacesCount + 2;
					for (int k = 0; k < innerSpacesCount; k++) line += " ";
					line += help[j];
					line += "\n";
				}
			}
			System.out.print(line);
		}
		return false;
	}

	public String getCommandName()
	{
		return this.commandName;
	}

	public void setCommandName(String name)
	{
		this.commandName = name;
		return;
	}

	public String[] getHelpMessage()
	{
		return this.helpMessage;
	}

	public void setHelpMessage(String[] helpMessage)
	{
		this.helpMessage = helpMessage;
		return;
	}
}