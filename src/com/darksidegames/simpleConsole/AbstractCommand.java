package com.darksidegames.simpleConsole;

/**
 * Represent a command.
 */
public abstract class AbstractCommand
{
	/**
	 * Called when the command is written.
	 * @param args
	 * The arguments given.
	 * @return
	 * {@code true} : stop the execution of the console.
	 * {@code false} : continue the execution of the console.
	 */
	public abstract boolean action(String[] args);
	public abstract String getCommandName();
	/**
	 * Define the new name of the command.
	 * 
	 * @param name
	 * the new name.
	 */
	public abstract void setCommandName(String name);
	public abstract String[] getHelpMessage();
	/**
	 * Define the help message for the command.
	 * 
	 * @param helpMessage
	 * The new help message.
	 */
	public abstract void setHelpMessage(String[] helpMessage);
}