package com.darksidegames.simpleConsole;

import java.util.ArrayList;
import java.io.Console;

import com.darksidegames.simpleConsole.command.ExitCommand;
import com.darksidegames.simpleConsole.command.HelpCommand;
import com.darksidegames.simpleConsole.command.EchoCommand;

public class SimpleConsole
{
	protected static String _startupMessage = "SimpleConsole";
	protected static String _prompt = "> ";
	protected static ArrayList<AbstractCommand> _commands = new ArrayList<AbstractCommand>();
	public static Console console;

	public static String getStartupMessage()
	{
		return SimpleConsole._startupMessage;
	}

	/**
	 * Set the console's startup message. Usually, this is the name of the console.
	 * 
	 * @param msg
	 * The message.
	 */
	public static void setStartupMessage(String msg)
	{
		SimpleConsole._startupMessage = "[" + msg + "]\n";
		return;
	}

	public static String getPrompt()
	{
		return SimpleConsole._prompt;
	}

	/**
	 * Set the console's prompt string.
	 * 
	 * @param prompt
	 * The new prompt.
	 */
	public static void setPrompt(String prompt)
	{
		SimpleConsole._prompt = prompt;
	}

	/**
	 * Register a new command in the console's commands list.
	 * 
	 * @param command
	 * The command instance
	 */
	public static void registerCommand(AbstractCommand command)
	{
		SimpleConsole._commands.add(command);
		return;
	}

	public static boolean isCommandRegistered(AbstractCommand command)
	{
		return SimpleConsole._commands.contains(command);
	}

	public static void unregisterCommand(AbstractCommand command)
	{
		if (SimpleConsole.isCommandRegistered(command))
			SimpleConsole._commands.remove(command);
		return;
	}

	public static ArrayList<AbstractCommand> getRegisteredCommands()
	{
		return SimpleConsole._commands;
	}

	public static void main(String[] args)
	{
		Keyboard.initKeyboard();
		console = System.console();
		// registering commands
		registerCommand(new ExitCommand());
		registerCommand(new HelpCommand());
		registerCommand(new EchoCommand());

		System.out.println(SimpleConsole.getStartupMessage());
		boolean stopExecution = false;
		while (!stopExecution) {
			System.out.print(SimpleConsole.getPrompt());
			String scannedLine = console.readLine();
			String[] commandSplited = scannedLine.split("\\s+");
			int i = 0;
			boolean commandFound = false;
			for (int cmdID = 0; cmdID < SimpleConsole.getRegisteredCommands().size(); cmdID++) {
				AbstractCommand cmd = SimpleConsole.getRegisteredCommands().get(cmdID);
				if (cmd.getCommandName().equals(commandSplited[0])) {
					commandFound = true;
					break;
				}
				i++;
			}
			if (commandFound) {
				String[] cmdArgs = new String[commandSplited.length - 1];
				for (int j = 1; j < commandSplited.length - 1; j++)
					cmdArgs[j - 1] = commandSplited[j];
				stopExecution = SimpleConsole.getRegisteredCommands().get(i).action(cmdArgs);
			} else {
				System.out.println("Error : command " + commandSplited[0] + " not found");
			}
		}
	}
}